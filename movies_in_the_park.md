---
layout: landing
title: 'Friday, July 19, 2024'
description: Movies In The Park, Leif Erikson Park, Duluth, MN
<!-- image: assets/images/pic01.jpg -->
nav-menu: true
show_tile: true
order: 1
---
The Northshore Philharmonic Orchestra, featuring guest conductor Dr. J. David Arnott, performs a selection of cinematic favorites preceding the showing of E.T. (1982) at Movies in the Park, starting at 8:00 p.m.

<!-- **Backup Location:**
In the event of poor weather conditions, the performance will take place at First Lutheran Church, 1100 E. Superior St., just across the street from Leif Erikson Park. -->

_Freewill Offerings accepted_
