---
layout: landing
title: 'Sunday, July 28, 2024'
description: Mitchell Auditorium, St. Scholastica, 1200 Kenwood Ave., Duluth, MN
<!-- image: assets/images/pic01.jpg -->
nav-menu: true
show_tile: true
order: 3
---

The Northshore Philharmonic Orchestra proudly presents Hector Berlioz's _Harold in Italy_ featuring guest soloist, Jonas Benson, conducted by guest conductor Dr. J. David Arnott.

Jonas Benson first appeared with the NSPO in 2021 playing Telemann’s Viola Concerto in G major. Home-grown in Duluth studying with Laurie Bastian, he received performance degrees from UM-Twin Cities and Northwestern University.
Jonas is a very prominent musician in Florida as Principal Violist of Sarasota Opera and St. Petersburg Opera and playing with the Florida Orchestra.

Additionally, Tracey Gibbens will conduct Tchaikovsky’s _Romeo and Juliet_ and the Sibelius _The Swan of Tuonela_.

Sunday, July 28 at 7:30 p.m. in Mitchell Auditorium at The College of Saint Scholastica.

Reception to follow concert.

_Freewill Offerings accepted_
