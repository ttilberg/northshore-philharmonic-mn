---
title: Interview with Viola Soloist Jonas Benson
layout: post
description: Jonas Benson
image: assets/images/jonas_viola345_orig.jpg
hide_image: true
show_tile: false
---

<div class="row">
  <div class="6u 12u$(small)">
    <span class="image">
      <img src="{{ site.baseurl }}/{{ page.image }}" alt="" />
    </span>
  </div>
  
  <div class="6u 12u$(small)">
    <p>
Former Duluthian and professional violist Jonas Benson is back in town to solo with the Northshore Philharmonic Orchestra. Jonas will guide us through the Italian landscapes with Hector Berlioz's <i>Harold in Italy</i> on July 23, 2024 at the Agate Encore Concert Series in Moose Lake, and on July 28, 2024 at the Mitchell Auditorium at The College of Saint Scholastica in Duluth.
    </p>
    <p>
Below is an excerpt from an interview with Jonas from 2021 when he performed the brilliant Telemann Viola Concerto with the Northshore Phil.
    </p>
  </div>
</div>
---

What first attracted you to music and what was your first instrument?

> Unlike many violists who switch from violin later on, I started playing the viola when I was six. I’m lucky to have supportive parents who exposed me to classical music in a way that was enjoyable and interesting. As a kid, I remember particularly enjoying the social aspect of music, and seeing it as a skill to be proud of, maybe in a more competitive way than I do now.


What draws you to make music?

> Music, especially live performance, is an incredible way to connect with audience members, colleagues, composers, and great art in a unique, one-time experience. Away from performance, I have more and more appreciation for practicing and teaching as outlets for creativity, expression, problem-solving, and continuous personal growth.


What influences did you have growing up that were important?

> Laurie Bastian was my viola teacher for eleven years, and she was certainly my biggest musical influence growing up. There was also a great community of mentors and friends in Duluth who pushed and inspired me to a higher standard. I particularly value the opportunities I had to play in ensembles. I always encourage students to play more chamber music, because it was so important to my development as a musician and human.


What draws you especially to the viola?

> It may be because I’ve played the viola for so long, but I enjoy the awareness, flexibility, control, and deep musical understanding it takes to be good at an inner voice instrument. Being in the middle of an ensemble gives a unique type of influence and perspective on music, and on how the ensemble operates as a whole. The viola’s sound is also fascinating and special to me. There’s more diversity in sound among violists and violas than in most instruments, and that expands the tone colors available to us and makes sound more personal and interesting. I’m currently playing a viola made by Marinos Glitsos that has given me a whole new world of sound to explore.


When you look back, what were some pivotal points in your career?

> The different places I’ve lived have given me positive experiences in different ways, so I see each move as an important pivot. Duluth and Minneapolis are where I grew up and developed as a student. Moving to Chicago for Northwestern University and the Civic Orchestra of Chicago led to collaborations with and exposure to some of the world’s best musicians. After not knowing what to expect when I went to play with the Florida Orchestra in 2016, St. Petersburg has been a great place to live, and where I’ve had the most professional success.
