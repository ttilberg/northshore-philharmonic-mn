---
title: Betsy Husby presents the Dvořák Cello Concerto (2023)
layout: post
description: Dr. Betsy Husby
image: assets/images/betsy_husby.jpeg
hide_image: true
show_tile: false
---

<div class="row">
  <div class="4u">
    <img src="{{ site.baseurl }}/{{ page.image }}" alt="" style="width:100%" />
  </div>

  <div class="8u">
    <h4>
      Dr. Betsy Husby, principal cellist of the Duluth-Superior Symphony Orchestra, presents the
      Cello Concerto in B minor by Antonín Dvořák with the Northshore Philharmonic Orchestra on Tuesday, July 25, 2023 in Moose Lake at the high school, and Tuesday, August 1, 2023 in Duluth at Mitchell Auditorium.
    </h4>
    <p>
      Betsy Husby earned her D.M.A. and M.M. degrees from SUNY at Stony Brook,
      L.I., N.Y. where she studied with Bernard Greenhouse, cellist of the
      internationally renowned Beaux Arts Trio, and Timothy Eddy of the Orion String
      quartet. Highlights of her career include performing in the 1986 Tchaikovsky
      Competition in Moscow, and twice being selected as semi-finalist among the top
      30 musicians in Minnesota for the McKnight Performing Artists Fellowship
      Awards.
    </p>
    <p>
      In New York, she was winner of the DMA concerto competition, a member of the
      Poulenc Chamber Players on Long Island, and principal cello of the Stony Brook
      graduate orchestra. Grants sponsored by Minnesota State Arts Board have taken
      her on recital tour in the United States, France, Japan, and Russia.
    </p>
    <p>
      Dr. Husby is principal cello (and often featured artist) with the Duluth Superior
      Symphony Orchestra, Lake Superior Chamber Orchestra, and other arts
      organizations in the region. She is a founding member of the Highland String
      Quartet.
    </p>
  </div>
</div>
