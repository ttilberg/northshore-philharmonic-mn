---
layout: landing
title: 'About the Orchestra'
description: Northshore Philharmonic Orchestra
image: assets/images/Tracey-Gibbens-wide.jpg
nav-menu: true
order: 100
---

In 2010, director Tracey Gibbens founded the NSPO along with Josh Aerie and Jim Welinski with a passion to draw together musicians of all ages to share brilliant orchestra music.

Our mission is to provide high-quality performances as Duluth’s only intergenerational and free summer orchestra. This totally home-grown group invites talented musicians from around the Northland – exceptional students, seasoned amateurs and professionals. We enjoy featuring powerful guest soloists.

For 14 seasons, audiences have enjoyed classical and pops concerts at venues like St. Scholastica, UWS-Superior, Moose Lake Community School, Minnesota Discovery Center, Movies in the Park, and the Tall Ships Festival.
