---
layout: page
title: 'Support the Orchestra'
description: Northshore Philharmonic Orchestra
image: assets/images/glockenspiel.jpg
nav-menu: true
order: 101
---
<div class="row">
  <div class="8u 12u$(small)">
    <div class="container" style="margin-right: 50px;">
      <h3>Community Supporters</h3>
      <p>
        We express profound appreciation for the many ways our community has contributed to making our 2023 Summer Season possible including:
      </p>

      <p>
        <strong>Agate Encores – Moose Lake Community Concerts Board</strong><br/>
        Jeanne Doty, Chair
      </p>

      <p>
        <strong>Duluth-Superior Symphony Orchestra</strong><br/>
        Brandon VanWaeyenberghe, Executive Director
      </p>

      <p>
        <strong>Downtown Duluth</strong><br/>
        Kristi Stokes, President/C.O.O.<br/>
        Melissa LaTour, Special Events Coordinator
      </p>

      <p>
        <strong>Schmitt Music/Duluth</strong><br/>
        Debbie Pederson, Store Manager
      </p>

      <p>
        <strong>The College of St. Scholastica</strong><br/>
        Dr. Jeremy Craycraft, Chair of Fine Arts, Associate Professor of Music<br/>
        Michele Runberg, Sales and Events Manager
      </p>

      <p>
        <strong>University of Wisconsin-Superior</strong><br/>
        Dr. Brett Jones, Retreating Chair, Music Department, and Percussion Professor<br/>
        Dr. Michael Fuchs, Ascending Chair, Music Department, and Choral Music Associate Professor 
      </p>
    </div>
  </div>

  <div class="4u 12u$(small) box align-center bg-alt rounded">
    <h3>Your Help Matters!</h3>
    <p>
      We gratefully receive
      free-will donations from
      our audiences
      (no admission charge).
    </p>

    <p>
      To make donations tax-deductible, please write a check to
      <br/>
      <strong>“Mesabi Symphony Orchestra”</strong>
      <br/>
      with a note that this is for NSPO. The MSO is our fiscal sponsor as a 501(c)(3) non-profit.
    </p>

    <p>
      To mail a donation, please send checks to:

      <br/>
      <strong>
        NSPO C/O Tracey Gibbens<br/>
        707 W 5th St<br/>
        Duluth, MN 55806 
      </strong>
    </p>

    <p>
      We also gladly accept contributions through Venmo in care of Kerry VanDusen.
      <a href="https://www.venmo.com/Kerry-VanDusen-2" title="Donate via Venmo" target="_new">
        <img src="{% link assets/images/venmo.png %}" alt="">
      </a>
    </p>
  </div>
</div>

