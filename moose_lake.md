---
layout: landing
title: 'Tuesday, July 23, 2024'
description: Moose Lake Community School, 313 Elm Ave., Moose Lake, MN
<!-- image: assets/images/pic01.jpg -->
nav-menu: true
show_tile: true
order: 2
---
The Northshore Philharmonic Orchestra arrives in Moose Lake for the [Agate Encores Community Concert series](https://agateencores.org), joined by guest soloist Jonas Benson to perform Hector Berlioz's _Harold in Italy_. Guest conductor Dr. J. David Arnott will guide us through Italian landscapes as we follow Harold in this musical journey.

Jonas Benson first appeared with the NSPO in 2021 playing Telemann’s Viola Concerto in G major. Home-grown in Duluth studying with Laurie Bastian, he received performance degrees from UM-Twin Cities and Northwestern University.
Jonas is a very prominent musician in Florida as Principal Violist of Sarasota Opera and St. Petersburg Opera and playing with the Florida Orchestra.

Additionally, Tracey Gibbens will conduct Tchaikovsky’s _Romeo and Juliet_ and the Sibelius _The Swan of Tuonela_.

Tuesday, July 23, 2024, 7:30 p.m. at the Moose Lake Community School.


_Freewill Offerings accepted_
